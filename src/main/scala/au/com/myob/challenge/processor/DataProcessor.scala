package au.com.myob.challenge.processor

import java.io.Writer

import au.com.myob.challenge.data.loaders.InputStreamLoader

import scala.util.Try

trait DataProcessor {

  def process(inputData: InputStreamLoader.Path, taxationData: InputStreamLoader.Path,
              writer: Writer): Try[Unit]

}
