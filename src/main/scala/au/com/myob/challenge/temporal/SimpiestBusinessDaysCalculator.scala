package au.com.myob.challenge.temporal

import java.time._
import java.time.temporal.ChronoUnit

class SimpiestBusinessDaysCalculator extends BusinessDaysCalculator {

  override def totalBusinessDays(year: Year): Int = 12

  override def numberOfBusinessDays(from: LocalDate, to: LocalDate): Int = 1
}
