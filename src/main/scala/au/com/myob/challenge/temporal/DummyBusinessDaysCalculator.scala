package au.com.myob.challenge.temporal

import java.time._
import java.time.temporal.ChronoUnit

class DummyBusinessDaysCalculator extends BusinessDaysCalculator {


  override def totalBusinessDays(year: Year): Int =
    numberOfBusinessDays(
      year.atMonthDay(MonthDay.of(1, 1)),
      year.atMonthDay(MonthDay.of(12, 31))
    )

  override def numberOfBusinessDays(from: LocalDate, to: LocalDate): Int = {
    if ( from.isAfter(to) ) throw new IllegalArgumentException("from > to")

    val daysInBetween = ChronoUnit.DAYS.between(from, to).toInt

    (0 to daysInBetween).foldLeft(0) { (result, day) ⇒
      result + (from.plusDays(day).getDayOfWeek match {
        case x if x == DayOfWeek.SATURDAY ⇒ 0
        case x if x == DayOfWeek.SUNDAY ⇒ 0
        case x ⇒ 1
      })
    }
  }
}
