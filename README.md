Required tools
=========================

- `SBT` (http://scala-sbt.org) used as a project building infrastructure provider

Overview
=========================

- Tests

- Run tests suite

`> sbt test`

- Build distributable package

`$> sbt universal:packageBin`
`$> cd target/universal`
`$> unzip myob-chlng-1.0.zip`

- Launch program

`$> ./bin/myob-chlng -taxationDataPath ../data/taxation-info.conf -file test.csv -output test-output.csv`

- Using days-based calculation strategy

`$> ./bin/myob-chlng -taxationData ../data/taxation-info.conf -file test.csv -businessDaysCalculator days -output test-output.csv`

- Help

`$>./bin/myob-chlng --help`

Design specifics
================

- Completely immutable application state; with only a single `var` used

- Responsibilities separation: highly specialized components


Implementation specifics
========================

Business days
========================
There is two different strategies implemented and available for business days calculation.

- *Day-based*

This strategy use only `business` days and ignore all weekends. But it doesn't take in account amount of
public holidays, so they will not be included in calculation.

Day based calculation make sense when input data contains partial periods (less or more then a month).

This strategy produces calculations slightly different from those in assignment description, because of
changing amount of business days from month to month.

- *Month based*

Simplest strategy that observes year as a 12months an each period as a 1/12 of the year. This strategy makes calculations
exactly the same as in assignment description.

Year of period
==========================

In the given example input *.csv it is not clear to which year a given record period belongs. So, based on this
uncertainty program will apply current financial year to all input data.

For a future versions, this option can either be represented on a data-level or as a part of program
configuration (launch arguments & config-based settings)

Taxation data
=========================

Application uses taxation data provided via *.conf file with data separated by financial year.

Each financial year has set of rules that application will use in order to calculate person tax rate and etc.


=====================================
Author: Cyril A. Karpenko <self@nikelin.ru>