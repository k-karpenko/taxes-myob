package au.com.myob.challenge.data.input

import java.io.{InputStream, InputStreamReader}
import java.time.{LocalDate, MonthDay, Year}
import java.time.format.DateTimeFormatter

import au.com.myob.challenge.data.{DataKeyField, DefaultCSVFormat}
import au.com.myob.challenge.domain.{PaymentPeriod, Person}
import com.github.tototoshi.csv.CSVReader

import scala.util.Try

object CSVInputDataLoader {

  type PaymentPeriodParser = String ⇒ PaymentPeriod

  def standardPaymentPeriodParser(value: String): PaymentPeriod = {
    val parts = value.replaceAll("–", ".").split("\\.")
    val startPeriod = MonthDay.parse(parts(0).trim, DateTimeFormatter.ofPattern("dd MMMM"))
    val endPeriod = MonthDay.parse(parts(1).trim, DateTimeFormatter.ofPattern("dd MMMM"))

    PaymentPeriod(
      Year.now().atMonthDay(startPeriod),
      Year.now().atMonthDay(endPeriod)
    )
  }

  case class Options(mappings: Map[DataKeyField, Int], paymentPeriodParser: PaymentPeriodParser)
}

class CSVInputDataLoader extends InputDataLoader {
  override type Format = CSVInputDataLoader.Options

  protected val csvFormat = new DefaultCSVFormat

  override def process(is: InputStream, options: CSVInputDataLoader#Format): Try[Stream[Person]] =
    Try {
      CSVReader.open(new InputStreamReader(is))(csvFormat)
        .toStream flatMap { line ⇒
        val firstNameOpt = line.lift(options.mappings.lift(DataKeyField.FirstName).getOrElse(0))
        val lastNameOpt = line.lift(options.mappings.lift(DataKeyField.LastName).getOrElse(1))
        val salaryOpt = line.lift(options.mappings.lift(DataKeyField.GrossIncome).getOrElse(2))
        val superRateOpt = line.lift(options.mappings.lift(DataKeyField.SuperAnnuation).getOrElse(3))
        val periodOpt = line.lift(options.mappings.lift(DataKeyField.Period).getOrElse(4))

        (firstNameOpt, lastNameOpt, periodOpt, salaryOpt, superRateOpt) match {
          case (firstName, lastName, Some(period), Some(salary), Some(superRate)) ⇒
            Some(
              Person(
                firstName.getOrElse("-"),
                lastName.getOrElse("-"),
                salary.toInt,
                superRate.replaceAll("%", "").toDouble / 100,
                options.paymentPeriodParser(period)
              )
            )
          case _ ⇒ None
        }
      }
    }
}
