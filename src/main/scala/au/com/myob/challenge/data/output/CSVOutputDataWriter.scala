package au.com.myob.challenge.data.output

import java.io.Writer
import java.time.format.DateTimeFormatter

import au.com.myob.challenge.data.{DataKeyField, DefaultCSVFormat}
import au.com.myob.challenge.domain.{Payslip, Person, PersonPayslip}
import com.github.tototoshi.csv.{CSVFormat, CSVWriter}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try
import scala.util.control.NonFatal

object CSVOutputDataWriter {

  case class Options(order: List[DataKeyField] = List.empty)

}

class CSVOutputDataWriter extends OutputDataWriter {
  override type Format = CSVOutputDataWriter.Options

  protected val csvFormat = new DefaultCSVFormat
  protected val dateFormatter = DateTimeFormatter.ofPattern("dd MMMM")

  override def outputData(records: Iterable[PersonPayslip], writer: Writer,
                          options: CSVOutputDataWriter#Format = CSVOutputDataWriter.Options()): Try[Unit] = {
    Try {
      val csvWriter = CSVWriter.open(writer)(csvFormat)

      records.zip(0 to records.size) foreach {
        case (PersonPayslip(person, record: Payslip.Record), idx) ⇒
          csvWriter.writeRow(Seq(
            idx.toString,
            "correct",
            recordDisplayValue(record, person, options.order.headOption.getOrElse(DataKeyField.FirstName)),
            recordDisplayValue(record, person, options.order.lift(1).getOrElse(DataKeyField.LastName)),
            recordDisplayValue(record, person, options.order.lift(2).getOrElse(DataKeyField.Period)),
            recordDisplayValue(record, person, options.order.lift(3).getOrElse(DataKeyField.GrossIncome)),
            recordDisplayValue(record, person, options.order.lift(3).getOrElse(DataKeyField.IncomeTax)),
            recordDisplayValue(record, person, options.order.lift(4).getOrElse(DataKeyField.NetIncome))
          ))
        case (PersonPayslip(person, record: Payslip.IncorrectData), idx) ⇒
          csvWriter.writeRow(Seq(
            idx.toString,
            "corrupted",
            record.failure.toString
          ))
      }

      csvWriter.close()
    }
  }

  private def recordDisplayValue(record: Payslip.Record, person: Person, key: DataKeyField): String =
      (key match {
        case DataKeyField.IncomeTax ⇒ record.incomeTax
        case DataKeyField.GrossIncome ⇒ record.grossIncome
        case DataKeyField.NetIncome ⇒ record.netIncome
        case DataKeyField.SuperAnnuation ⇒ record.superPart
        case DataKeyField.FirstName ⇒ person.firstName
        case DataKeyField.LastName ⇒ person.lastName
        case DataKeyField.Period ⇒ dateFormatter.format(person.period.from) + " - " + dateFormatter.format(person.period.to)
      }).toString
}
