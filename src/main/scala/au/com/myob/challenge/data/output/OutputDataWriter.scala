package au.com.myob.challenge.data.output

import java.io.Writer

import au.com.myob.challenge.domain.{PersonPayslip}

import scala.util.Try

trait OutputDataWriter {
  type Format

  def outputData(records: Iterable[PersonPayslip], writer: Writer,
                 options: CSVOutputDataWriter#Format): Try[Unit]

}
