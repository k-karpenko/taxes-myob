package au.com.myob.challenge

import java.time.LocalDate

import au.com.myob.challenge.util.FailureType

package object domain {

  case class SalaryRange(from: Int, to: Option[Int]) {
    def contains(salary: Int) = {
      salary >= from && to.forall(v ⇒ salary <= v)
    }
  }

  case class TaxationRule(salaryRange: SalaryRange, fixedDeduction: Int,
                          taxationRate: Double)

  case class TaxationData(financialYear: PaymentPeriod, rules: Seq[TaxationRule])

  case class Person(firstName: String,
                    lastName: String,
                    annualSalary: Int,
                    superRate: Double,
                    period: PaymentPeriod)

  case class PersonPayslip(p: Person, payslip: Payslip)

  sealed trait Payslip
  object Payslip {

    case class Record(grossIncome: Int,
                      incomeTax: Int,
                      netIncome: Int,
                      superPart: Int) extends Payslip

    case class IncorrectData(failure: FailureType) extends Payslip

  }

  object PaymentPeriod {
    def of(from: LocalDate, to: LocalDate): PaymentPeriod = {
      if ( from.isAfter(to) ) throw new IllegalArgumentException
      else PaymentPeriod(from, to)
    }
  }

  case class PaymentPeriod(from: LocalDate, to: LocalDate) {
    def isInRange(period: PaymentPeriod): Boolean = {
      if ( from.isAfter(period.from) ) false
      else if ( to.isBefore(period.to) ) false
      else true
    }
  }

}
