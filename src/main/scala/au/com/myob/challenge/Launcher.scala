package au.com.myob.challenge

import java.io.{File, FileWriter, InputStreamReader}

import au.com.myob.challenge.data.DefaultCSVFormat
import au.com.myob.challenge.data.input.CSVInputDataLoader
import au.com.myob.challenge.data.loaders.{ClasspathStreamLoader, CompositeStreamLoader, FilesystemBasedStreamLoader, InputStreamLoader}
import au.com.myob.challenge.data.output.CSVOutputDataWriter
import au.com.myob.challenge.domain.{Person, TaxationData}
import au.com.myob.challenge.processor.DefaultDataProcessor
import au.com.myob.challenge.taxes.income.DefaultTaxCalculator
import au.com.myob.challenge.taxes.rules.ConfigBasedTaxationDataLoader
import au.com.myob.challenge.temporal.{DummyBusinessDaysCalculator, SimpiestBusinessDaysCalculator}

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._

object Launcher {

  import util._

  private val defaultTaxationDataPath = InputStreamLoader.Path.ClasspathBased("info.conf")
  private val defaultBusinessDaysCalculator = "months"

  implicit val ec = ExecutionContext.global

  private val streamLoader = new CompositeStreamLoader(
    Seq(
      new ClasspathStreamLoader(),
      new FilesystemBasedStreamLoader()
    )
  )

  def main(args: Array[String]): Unit = {
    try {
      start(args)
    } catch {
      case e: Throwable ⇒
        System.err.println("= ERROR")
    }
  }

  def start(args: Array[String]): Unit = {
    val options = parseOpts(args)

    val isHelp =
      options.get("--help").nonEmpty

    if ( isHelp ) {
      println("Taxes Calculator")
      println("---------------------------------------------------------------------------------")
      println("./bin/pay -file /opt/input-data.csv -output /opt/payslips-data.csv               ")
      println("---------------------------------------------------------------------------------")
      println("-taxationDataPath [/path/to/data]      - change default taxation rate settings")
      println("-file [/path/to/data]                  - path to file with input data to process")
      println("-businessDaysCalculator [months|days]  - strategy to decide how many paid business days")
      println("                                         occurred during a given period (for partial periods calculation)")
      println("-verbose                               - enable verbose output")
      println("-output [/path/to/data]                - path to file with input data to process")
      System.exit(0)
    }

    val isVerbose =
      options.get("--verbose").nonEmpty

    val businessDaysStrategy = options.getOrElse("-businessDaysCalculator", defaultBusinessDaysCalculator) match {
      case "days" ⇒ new DummyBusinessDaysCalculator
      case "months" ⇒ new SimpiestBusinessDaysCalculator
      case x: String ⇒
        System.err.println("[!] Unknown days calculation strategy provided: " + x )
        throw new Exception
    }

    val taxationDataPath =
      options.get("-taxationData")
        .map(InputStreamLoader.Path.FilesystemBased)
        .getOrElse(defaultTaxationDataPath)

    if ( isVerbose ) {
      println(s"taxationDataPath = $taxationDataPath")
    }

    val inputDataPath = InputStreamLoader.Path.FilesystemBased(
      options.getOrElse("-file", {
        System.err.println("[!] No input file provided")
        throw new Exception()
      })
    )

    if ( isVerbose ) {
      println(s"inputDataPath = $inputDataPath")
    }

    val outputDataFile = new File(
      options.getOrElse("-output", {
        System.err.println("[!] No output path provided")
        throw new Exception()
      })
    )

    if ( !outputDataFile.getParentFile.exists() ) {
      System.err.println("[!] Output data path does not exist")
      System.exit(1)
    }

    val dataProcessor = new DefaultDataProcessor(streamLoader,
      taxationData ⇒ new DefaultTaxCalculator(taxationData,
        businessDaysStrategy, verbose = isVerbose),
      verbose = isVerbose
    )

    dataProcessor.process(inputDataPath, taxationDataPath, new FileWriter(outputDataFile))
  }

}
