package au.com.myob.challenge.data.loaders

import java.io.InputStream

import scala.util.{Failure, Try}
import scala.util.control.NonFatal

class ClasspathStreamLoader extends InputStreamLoader {
  import InputStreamLoader._

  override def isAcceptable(path: Path) =
    path match {
      case x: Path.ClasspathBased ⇒ true
      case _ ⇒ false
    }

  override def openStream(path: Path): Try[InputStream] =
    path match {
      case x: Path.ClasspathBased ⇒
        Try {
          val stream = Option(getClass.getResourceAsStream(x.value))
            .getOrElse(getClass.getClassLoader.getResourceAsStream(x.value))

          if ( stream != null ) stream
          else
            throw new IllegalStateException(s"failed to resolve resource: ${x.value}")
        }
      case path: Any ⇒ Failure(new IllegalStateException(s"unsupported path provided: $path"))
    }
}
