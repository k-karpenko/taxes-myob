package au.com.myob.challenge

package object data {
  sealed trait DataKeyField

  object DataKeyField {
    case object FirstName extends DataKeyField
    case object LastName extends DataKeyField
    case object Period extends DataKeyField
    case object GrossIncome extends DataKeyField
    case object NetIncome extends DataKeyField
    case object SuperAnnuation extends DataKeyField
    case object IncomeTax extends DataKeyField
  }

  class DefaultCSVFormat extends com.github.tototoshi.csv.DefaultCSVFormat
}
