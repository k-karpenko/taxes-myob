package au.com.myob.challenge.tax

import java.math.MathContext
import java.time.{LocalDate, MonthDay, Year}
import java.time.temporal.ChronoUnit

import au.com.myob.challenge.domain._
import au.com.myob.challenge.taxes.income.{DefaultTaxCalculator, IncomeTaxCalculator}
import au.com.myob.challenge.temporal.{BusinessDaysCalculator, DummyBusinessDaysCalculator}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{Matchers, WordSpec}

class DefaultTaxCalculatorTest extends WordSpec with Matchers with MockFactory {

  "2012-2013 provider" when {
    "relevant period requested" should {
      "generate payslip with a correct income tax rate in it " when {
        "salary in between from 0 to 18.200" in new Fixture {
          withBusinessDaysCalculator { calculator ⇒
            withTaxationData { taxationData ⇒
              withProvider(taxationData, calculator) { provider ⇒
                (0 to 18200) foreach { salary ⇒
                  val totalBusinessDays = calculator.totalBusinessDays(Year.of(correctPeriod.from.getYear))
                  val businessDays = calculator.numberOfBusinessDays(correctPeriod.from, correctPeriod.to)
                  val salaryPerDay =  salary / totalBusinessDays

                  val result = provider.computeTax(salary, testSuperAnnuation1, correctPeriod)
                  result.isRight should be(true)

                  result.right.get match {
                    case p: Payslip.IncorrectData ⇒
                      fail("Payslip generation failed", p.failure)
                    case p: Payslip.Record ⇒
                      val baseTaxPart = BigDecimal(0)
                      val leveledPart = BigDecimal(0)
                      val incomeTax = (baseTaxPart + leveledPart) / totalBusinessDays * businessDays
                      val gross = businessDays * salaryPerDay
                      val superPart = (gross - incomeTax) * testSuperAnnuation1

                      p.grossIncome should be ( gross )
                      p.incomeTax should be (incomeTax.round(testMathContext).toInt)
                      p.superPart should be (superPart.round(testMathContext).toInt)
                  }
                }
              }
            }
          }
        }

        "salary in between from 18.201 to 37.000" in new Fixture {
          withBusinessDaysCalculator { calculator ⇒
            withTaxationData { taxationData ⇒
              withProvider(taxationData, calculator) { provider ⇒
                (18201 to 37000) foreach { salary ⇒
                  val totalBusinessDays = calculator.totalBusinessDays(Year.of(correctPeriod.from.getYear))
                  val businessDays = calculator.numberOfBusinessDays(correctPeriod.from, correctPeriod.to)
                  val salaryPerDay =  salary / totalBusinessDays

                  val result = provider.computeTax(salary, testSuperAnnuation1, correctPeriod)
                  result.isRight should be(true)

                  result.right.get match {
                    case p: Payslip.IncorrectData ⇒
                      fail("Payslip generation failed", p.failure)
                    case p: Payslip.Record ⇒
                      val baseTaxPart = BigDecimal(0)
                      val leveledPart = BigDecimal(salary - 18201) * 0.19
                      val incomeTax = (baseTaxPart + leveledPart) / totalBusinessDays * businessDays
                      val gross = businessDays * salaryPerDay
                      val superPart = (gross - incomeTax) * testSuperAnnuation1

                      p.grossIncome should be ( gross )
                      p.incomeTax should be (incomeTax.round(testMathContext).toInt)
                      p.superPart should be (superPart.round(testMathContext).toInt)
                  }
                }
              }
            }
          }
        }

        "salary in between from 37.001 to 80.000" in new Fixture {
          withBusinessDaysCalculator { calculator ⇒
            withTaxationData { taxationData ⇒
              withProvider(taxationData, calculator) { provider ⇒
                (37001 to 80000) foreach { salary ⇒
                  val totalBusinessDays = calculator.totalBusinessDays(Year.of(correctPeriod.from.getYear))
                  val businessDays = calculator.numberOfBusinessDays(correctPeriod.from, correctPeriod.to)
                  val salaryPerDay =  salary / totalBusinessDays

                  val result = provider.computeTax(salary, testSuperAnnuation1, correctPeriod)
                  result.isRight should be(true)

                  result.right.get match {
                    case p: Payslip.IncorrectData ⇒
                      fail("Payslip generation failed", p.failure)
                    case p: Payslip.Record ⇒
                      val baseTaxPart = BigDecimal(3572)
                      val leveledPart = BigDecimal(salary - 37001) * 0.325
                      val incomeTax = (baseTaxPart + leveledPart) / totalBusinessDays * businessDays
                      val gross = businessDays * salaryPerDay
                      val superPart = (gross - incomeTax) * testSuperAnnuation1

                      p.grossIncome should be ( gross )
                      p.incomeTax should be (incomeTax.round(testMathContext).toInt)
                      p.superPart should be (superPart.round(testMathContext).toInt)
                  }
                }
              }
            }
          }
        }

        "salary in between from 80.001 to 180.000" in new Fixture {
          withBusinessDaysCalculator { calculator ⇒
            withTaxationData { taxationData ⇒
              withProvider(taxationData, calculator) { provider ⇒
                (80001 to 180000) foreach { salary ⇒
                  val totalBusinessDays = calculator.totalBusinessDays(Year.of(correctPeriod.from.getYear))
                  val businessDays = calculator.numberOfBusinessDays(correctPeriod.from, correctPeriod.to)
                  val salaryPerDay =  salary / totalBusinessDays

                  val result = provider.computeTax(salary, testSuperAnnuation1, correctPeriod)
                  result.isRight should be(true)

                  result.right.get match {
                    case p: Payslip.IncorrectData ⇒
                      fail("Payslip generation failed", p.failure)
                    case p: Payslip.Record ⇒
                      val baseTaxPart = 17574
                      val leveledPart = BigDecimal(salary - 80001) * 0.37
                      val incomeTax = (baseTaxPart + leveledPart) / totalBusinessDays * businessDays
                      val gross = businessDays * salaryPerDay
                      val superPart = (gross - incomeTax) * testSuperAnnuation1

                      p.grossIncome should be ( gross )
                      p.incomeTax should be (incomeTax.round(testMathContext).toInt)
                      p.superPart should be (superPart.round(testMathContext).toInt)
                  }
                }
              }
            }
          }
        }

        "salary is more than 180.000" in new Fixture {
          withBusinessDaysCalculator { calculator ⇒
            withTaxationData { taxationData ⇒
              withProvider(taxationData, calculator) { provider ⇒
                (180001 to 500000) foreach { salary ⇒
                  val totalBusinessDays = calculator.totalBusinessDays(Year.of(correctPeriod.from.getYear))
                  val businessDays = calculator.numberOfBusinessDays(correctPeriod.from, correctPeriod.to)
                  val salaryPerDay =  salary / totalBusinessDays

                  val result = provider.computeTax(salary, testSuperAnnuation1, correctPeriod)
                  result.isRight should be(true)

                  result.right.get match {
                    case p: Payslip.IncorrectData ⇒
                      fail("Payslip generation failed", p.failure)
                    case p: Payslip.Record ⇒
                      val baseTaxPart = 54547
                      val leveledPart = BigDecimal(salary - 180001) * 0.45
                      val incomeTax = (baseTaxPart + leveledPart) / totalBusinessDays * businessDays
                      val gross = businessDays * salaryPerDay
                      val superPart = (gross - incomeTax) * testSuperAnnuation1

                      p.grossIncome should be ( gross )
                      p.incomeTax should be (incomeTax.round(testMathContext).toInt)
                      p.superPart should be (superPart.round(testMathContext).toInt)
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  trait Fixture {
    val testMathContext = new MathContext(0, java.math.RoundingMode.HALF_UP)

    val testTaxationData = Seq(
      TaxationData(
        financialYear = PaymentPeriod.of(
          Year.of(2015).atMonthDay(MonthDay.of(6, 1)),
          Year.of(2016).atMonthDay(MonthDay.of(6, 1))
        ),
        rules = Seq(
          TaxationRule(
            salaryRange = SalaryRange(0, Some(18200)),
            fixedDeduction = 0,
            taxationRate = 0
          ),
          TaxationRule(
            salaryRange = SalaryRange(18201, Some(37000)),
            fixedDeduction = 0,
            taxationRate = 0.19
          ),
          TaxationRule(
            salaryRange = SalaryRange(37001, Some(80000)),
            fixedDeduction = 3572,
            taxationRate = 0.325
          ),
          TaxationRule(
            salaryRange = SalaryRange(80001, Some(180000)),
            fixedDeduction = 17574,
            taxationRate = 0.37
          )
          ,
          TaxationRule(
            salaryRange = SalaryRange(180001, None),
            fixedDeduction = 54547,
            taxationRate = 0.45
          )
        )
      )
    )

    def withProvider(taxationData: Seq[TaxationData],
                     businessDaysCalculator: BusinessDaysCalculator)
                    (block: IncomeTaxCalculator ⇒ Unit): Unit = {
      block(new DefaultTaxCalculator(taxationData, businessDaysCalculator))
    }

    def withTaxationData(block: Seq[TaxationData] ⇒ Unit): Unit = {
      block(testTaxationData)
    }

    def withBusinessDaysCalculator(block: BusinessDaysCalculator ⇒ Unit): Unit = {
      val obj = stub[BusinessDaysCalculator]
      (obj.totalBusinessDays _).when(*).returns(12)
      (obj.numberOfBusinessDays _).when(*, *).returns(1)
      block(obj)
    }

    val testSuperAnnuation1 = 0.09

    val correctPeriod = PaymentPeriod(LocalDate.of(2015, 7, 1), LocalDate.of(2015, 7, 31))
  }

}
