package au.com.myob.challenge.taxes.income

import au.com.myob.challenge.domain.{PaymentPeriod, Payslip}
import au.com.myob.challenge.util.FailureType

trait IncomeTaxCalculator {

  def computeTax(salary: Int, superAnnuation: Double, period: PaymentPeriod): Either[FailureType, Payslip]

}
