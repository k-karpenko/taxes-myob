package au.com.myob.challenge.processor

import java.io._

import au.com.myob.challenge.data.input.CSVInputDataLoader
import au.com.myob.challenge.data.loaders.InputStreamLoader
import au.com.myob.challenge.data.loaders.InputStreamLoader.Path
import au.com.myob.challenge.data.output.CSVOutputDataWriter
import au.com.myob.challenge.domain.{Payslip, Person, PersonPayslip, TaxationData}
import au.com.myob.challenge.taxes.income.IncomeTaxCalculator
import au.com.myob.challenge.taxes.rules.ConfigBasedTaxationDataLoader

import scala.util.Try
import scala.util.control.NonFatal

class DefaultDataProcessor(inputStreamLoader: InputStreamLoader,
                           incomeTaxCalculatorProvider: Seq[TaxationData] ⇒ IncomeTaxCalculator,
                           verbose: Boolean = false)
  extends DataProcessor {

  override def process(inputDataPath: Path, taxationDataPath: Path, writer: Writer): Try[Unit] = {
    val taxationDataEither =
      loadTaxationData(taxationDataPath)
        .recover { case e if NonFatal(e) ⇒
          if ( verbose ) {
            System.err.println("[!] Failure during taxation data processing: ")
            e.printStackTrace()
          }
          throw e
        }

    val inputDataEither = loadInputData(inputDataPath)
      .recover { case e if NonFatal(e) ⇒
        if ( verbose ) {
          System.err.println("[!] Failure during input data processing")
          e.printStackTrace()
        }
        throw e
      }

    for {
      taxationData ← taxationDataEither
      inputData ← inputDataEither
    } yield {
      val incomeTaxCalculator = incomeTaxCalculatorProvider(taxationData)

      val payslips = inputData map { person ⇒
        PersonPayslip(person,
          incomeTaxCalculator.computeTax(person.annualSalary, person.superRate, person.period) match {
            case Left(e) ⇒ Payslip.IncorrectData(e)
            case Right(payslip) ⇒ payslip
          }
        )
      }

      val outputDataWriter = new CSVOutputDataWriter()

      outputDataWriter.outputData(payslips, writer) recover {
        case e: Throwable ⇒
          System.err.println("[!] Failed to output generate payslips report")
          if ( verbose ) {
            e.printStackTrace()
          }
          throw e
      }
    }
  }

  def loadInputData(path: InputStreamLoader.Path): Try[Stream[Person]] = {
    inputStreamLoader.openStream(path) flatMap { is ⇒
      val inputDataLoader = new CSVInputDataLoader()
      inputDataLoader.process( is,
        CSVInputDataLoader.Options(
          Map(),
          CSVInputDataLoader.standardPaymentPeriodParser
        )
      )
    }
  }

  def loadTaxationData(path: InputStreamLoader.Path): Try[Seq[TaxationData]] = {
    inputStreamLoader.openStream(path) map { is ⇒
        val taxationDataLoader = new ConfigBasedTaxationDataLoader()
        taxationDataLoader.loadTaxationData(new InputStreamReader(is))
    }
  }
}
