package au.com.myob.challenge.data.loaders

import scala.util.Failure

class CompositeStreamLoader(loaders: Seq[InputStreamLoader])
  extends InputStreamLoader {
  import InputStreamLoader._

  require(loaders.nonEmpty)

  override def isAcceptable(path: Path) = loaders.exists(_.isAcceptable(path))

  override def openStream(path: Path) =
    loaders
      .find(_.isAcceptable(path))
      .map(_.openStream(path))
      .getOrElse(
        Failure(new IllegalStateException(s"no stream loader available: $path"))
      )
}
