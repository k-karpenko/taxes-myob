name := "myob-chlng"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
libraryDependencies += "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % "test"
libraryDependencies += "com.typesafe" % "config" % "1.3.0"
libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.3"

enablePlugins(JavaAppPackaging)

topLevelDirectory := Some("taxes-calculator")

mappings in Universal ++= Seq(
  file("src/main/resources/info.conf") → "./data/taxation-info.conf"
)

lazy val ClasspathPattern = "declare -r app_classpath=\"(.*)\"\n".r

bashScriptDefines :=  bashScriptDefines.value.map {
  case ClasspathPattern(classpath) => "declare -r app_classpath=\"${app_home}/../data/*:" + classpath + "\"\n"
  case _@entry => entry
}
