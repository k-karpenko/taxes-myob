package au.com.myob.challenge.temporal

import java.time.{LocalDate, Year}

import org.scalatest.{Matchers, WordSpec}

class DummyBusinessDaysCalculatorTest extends WordSpec with Matchers {

  "DummyBusinessDaysCalculator" should {
    "return correct result" when {
      "amount of total business days requested" in new Fixture {
        withDaysCalculator { calculator ⇒
          calculator.totalBusinessDays(Year.of(2014)) should be (261)
        }
      }

      "partial period provided" in new Fixture {
        withDaysCalculator { calculator ⇒
          calculator.numberOfBusinessDays(LocalDate.of(2014, 1, 1), LocalDate.of(2014, 1, 15)) should be (11)
        }
      }

      "whole period provided" in new Fixture {
        withDaysCalculator { calculator ⇒
          calculator.numberOfBusinessDays(LocalDate.of(2014, 1, 1), LocalDate.of(2014, 1, 31)) should be (23)
        }
      }
    }

    "throw exception when `from` is after `to`" in new Fixture {
      withDaysCalculator { calculator ⇒
        the[IllegalArgumentException] thrownBy {
          calculator.numberOfBusinessDays(LocalDate.of(2014, 1, 31), LocalDate.of(2014, 1, 1))
        }
      }
    }
  }

  trait Fixture {

    def withDaysCalculator(block: BusinessDaysCalculator ⇒ Unit): Unit = {
      block(new DummyBusinessDaysCalculator)
    }

  }

}
