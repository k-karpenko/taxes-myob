package au.com.myob.challenge.tax

import java.io.{InputStreamReader, Reader}
import java.time.LocalDate

import au.com.myob.challenge.domain.SalaryRange
import au.com.myob.challenge.taxes.rules.{ConfigBasedTaxationDataLoader, TaxationDataLoader}
import org.scalatest.{Matchers, WordSpec}

class ConfigBasedTaxDataLoaderTest extends WordSpec with Matchers {

  "ConfigBasedTaxDataLoader" should {
    "correctly read configuration file" when {
      "single year data provided" in new Fixture {
        withConfigBasedDataLoader { loader ⇒
          withTestDataReader { reader ⇒
            val taxationData = loader.loadTaxationData(reader)
            taxationData should have size 1
            taxationData.head.rules should have size 5
            taxationData.head.financialYear.from should be (LocalDate.of(2015, 6, 1))
            taxationData.head.financialYear.to should be (LocalDate.of(2016, 6, 1))

            val rules = taxationData.head.rules
            rules.head.salaryRange shouldBe SalaryRange(0, Some(18000))
            rules.head.taxationRate shouldBe 0
            rules.head.fixedDeduction shouldBe 0

            rules(1).salaryRange shouldBe SalaryRange(18001, Some(37000))
            rules(1).taxationRate shouldBe 0.19
            rules(1).fixedDeduction shouldBe 0

            rules(2).salaryRange shouldBe SalaryRange(37001, Some(80000))
            rules(2).taxationRate shouldBe 0.325
            rules(2).fixedDeduction shouldBe 3572

            rules(3).salaryRange shouldBe SalaryRange(80001, Some(180000))
            rules(3).taxationRate shouldBe 0.37
            rules(3).fixedDeduction shouldBe 17547

            rules(4).salaryRange shouldBe SalaryRange(180001, None)
            rules(4).taxationRate shouldBe 0.45
            rules(4).fixedDeduction shouldBe 54547
          }
        }
      }
    }
  }

  trait Fixture {
    def withTestDataReader(block: Reader ⇒ Unit): Unit = {
      block(new InputStreamReader(getClass.getResourceAsStream("/db/info.conf")))
    }

    def withConfigBasedDataLoader(block: TaxationDataLoader ⇒ Unit): Unit = {
      block(new ConfigBasedTaxationDataLoader)
    }
  }

}
