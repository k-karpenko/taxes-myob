package au.com.myob.challenge.taxes.income

import java.math.MathContext
import java.time.Year

import au.com.myob.challenge.domain.{PaymentPeriod, Payslip, TaxationData}
import au.com.myob.challenge.temporal.BusinessDaysCalculator
import au.com.myob.challenge.util.FailureType

class DefaultTaxCalculator(taxationData: Seq[TaxationData],
                           businessDaysCalculator: BusinessDaysCalculator,
                           verbose: Boolean = false)
  extends IncomeTaxCalculator {

  private final val mathContext = new MathContext(0, java.math.RoundingMode.HALF_UP)

  protected def baseDeductible(data: TaxationData, salary: Int): Option[Int] =
    data.rules.find(_.salaryRange.contains(salary)) map (_.fixedDeduction)


  protected def leveledDeductible(data: TaxationData, salary: Int): Either[FailureType, BigDecimal] = {
      data.rules
        .find(r ⇒ r.salaryRange.to.forall(v ⇒ v >= salary) && r.salaryRange.from <= salary)
        .map { rule ⇒
          Right(BigDecimal(salary - rule.salaryRange.from) * rule.taxationRate)
        }
        .getOrElse(Left(FailureType.TaxationBaseCorrupted))
  }

  override def computeTax(salary: Int, superAnnuation: Double,
                          period: PaymentPeriod): Either[FailureType, Payslip] = {
    taxationData.find(_.financialYear.isInRange(period)) match {
      case Some(taxationDataRecord) ⇒

        val totalBusinessDays = businessDaysCalculator.totalBusinessDays(Year.of(period.from.getYear))
        val businessDays = businessDaysCalculator.numberOfBusinessDays(period.from, period.to)
        val salaryPerDay = BigDecimal(salary / totalBusinessDays)

        if (salary < 0)
          Left(FailureType.NegativeValue)
        else {
          val basePartOpt = baseDeductible(taxationDataRecord, salary)
          val leveledPartOpt = leveledDeductible(taxationDataRecord, salary)

          Right(
            (basePartOpt, leveledPartOpt) match {
              case (Some(basePart), Right(leveledPart)) ⇒


                val grossSalary = businessDays * salaryPerDay
                val taxes = (basePart + leveledPart) / totalBusinessDays * businessDays
                val netIncome = grossSalary - taxes
                val superAnnuationPart = netIncome * superAnnuation

                Payslip.Record(
                  rounded(grossSalary),
                  rounded(taxes),
                  rounded(netIncome),
                  rounded(superAnnuationPart)
                )
              case _ ⇒ Payslip.IncorrectData(FailureType.TaxationBaseCorrupted)
            }
          )
        }

      case None ⇒
        if (verbose) System.err.println("[!] Cannot find rules for the year " + period.from.getYear)
        Left(FailureType.TaxationBaseCorrupted)
    }
  }

  protected def rounded(value: BigDecimal): Int = value.round(mathContext).toInt

}
