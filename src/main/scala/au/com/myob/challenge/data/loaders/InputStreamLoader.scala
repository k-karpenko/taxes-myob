package au.com.myob.challenge.data.loaders

import java.io.InputStream

import scala.util.Try

object InputStreamLoader {

  sealed trait Path
  object Path {
    case class ClasspathBased(value: String) extends Path
    case class FilesystemBased(value: String) extends Path
  }

}

trait InputStreamLoader {
  import InputStreamLoader._

  def isAcceptable(path: Path): Boolean

  def openStream(path: Path): Try[InputStream]

}
