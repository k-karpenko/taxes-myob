package au.com.myob.challenge.taxes.rules

import java.io.Reader

import scala.concurrent.{ExecutionContext, Future}

trait TaxationDataLoader {
  import au.com.myob.challenge.domain._

  def loadTaxationData(stream: Reader): Seq[TaxationData]

}
