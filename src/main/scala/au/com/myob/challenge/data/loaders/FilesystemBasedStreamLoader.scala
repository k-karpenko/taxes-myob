package au.com.myob.challenge.data.loaders

import java.io.FileInputStream

import au.com.myob.challenge.data.loaders.InputStreamLoader.Path

import scala.util.{Failure, Try}
import scala.util.control.NonFatal

class FilesystemBasedStreamLoader extends InputStreamLoader {
  override def isAcceptable(path: Path) =
    path match {
      case _: Path.FilesystemBased ⇒ true
      case _ ⇒ false
    }

  override def openStream(path: Path) =
    path match {
      case path: Path.FilesystemBased ⇒
        Try {
          new FileInputStream(path.value)
        }
      case _ ⇒ Failure(new IllegalArgumentException("not supported"))
    }
}
