package au.com.myob.challenge.data.input

import java.io.InputStream

import au.com.myob.challenge.domain.Person

import scala.util.Try

trait InputDataLoader {

  type Format

  def process(is: InputStream, format: Format): Try[Stream[Person]]

}
