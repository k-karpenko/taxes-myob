package au.com.myob.challenge.taxes.rules

import java.io.Reader
import java.time.{LocalDate, Year}

import au.com.myob.challenge.data.loaders.InputStreamLoader
import com.typesafe.config.ConfigFactory

import scala.collection.JavaConversions._
import scala.concurrent.{ExecutionContext, Future}

class ConfigBasedTaxationDataLoader extends TaxationDataLoader {
  import au.com.myob.challenge.domain._

  override def loadTaxationData(reader: Reader): Seq[TaxationData] = {
    val config = ConfigFactory.parseReader(reader)

    config.getConfigList("years").toSeq map { yearConfig ⇒
      val period = yearConfig.getConfig("period")

      val startOfPeriod = period.getConfig("start")
      val endOfPeriod = period.getConfig("end")

      val financialYear = PaymentPeriod(
        LocalDate.of(startOfPeriod.getInt("year"), startOfPeriod.getInt("month"), startOfPeriod.getInt("day")),
        LocalDate.of(endOfPeriod.getInt("year"), startOfPeriod.getInt("month"), startOfPeriod.getInt("day"))
      )

      TaxationData(financialYear, yearConfig.getConfigList("rules").toSeq map { rule ⇒
        val salaryRange = rule.getConfig("salaryRange")

        TaxationRule(
          SalaryRange(salaryRange.getInt("from"),
            if(salaryRange.getInt("to") < 0) None else Some(salaryRange.getInt("to")) ),
          rule.getInt("fixedBase"),
          rule.getDouble("taxationRate")
        )
      })
    }
  }
}
