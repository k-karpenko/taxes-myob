package au.com.myob.challenge.temporal

import java.time.{LocalDate, Year}

trait BusinessDaysCalculator {

  def totalBusinessDays(year: Year): Int

  def numberOfBusinessDays(from: LocalDate, to: LocalDate): Int

}
